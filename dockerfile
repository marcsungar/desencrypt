FROM openjdk:17
COPY target/desencrypter-1.0.jar /desencrypter-1.0.jar
ENTRYPOINT ["java","-jar","/desencrypter-1.0.jar"]